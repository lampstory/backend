<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('POST');

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;

    if(empty($_POST['token'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "token"'];
        $errors++;
    }

    if(empty($_POST['name'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "name"'];
        $errors++;
    }

    if(empty($_POST['text'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "text"'];
        $errors++;
    }

    $user_id = get_user_by_token($_POST['token'], true);

    if(!$user_id) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует'];
        $errors++;
    }

    if($errors==0) {
        $name = $_POST['name'];
        $text = $_POST['text'];
        $password = !empty($_POST['password']) ? "'".md5($_POST['photo'])."'" : "NULL";
        $tag = $_POST['tag']??null;

        if($_FILES['photo']) {
            $photo = upload_file('post', 'photo');
            if(gettype($photo) <> 'string') {
                $data = $photo;
            } else {
                $photo = "'".$photo."'";
            }
        } else {
            $photo = "'".$_POST['photo']."'"??"NULL";
        }

        if($photo=="NULL" OR gettype($photo) == 'string') {
            $post = $connect->query("INSERT INTO post VALUES(DEFAULT, '$name', DEFAULT, $photo, '$text', DEFAULT, $password, '$user_id')")??false;
            if($post) {
                if($tag) {
                    $post_id = $connect->insert_id;
                    $tags = explode(",", $tag);
                    foreach ($tags as $tag_id) {
                        $connect->query('INSERT INTO post_tags VALUES(DEFAULT, '.$post_id.', '.$tag_id.')');
                    }
                }
                $data = ['status' => 200];
            } else {
                $data = ['status' => 500];
            }
        }
    }

    $connect->close();
}
echo json_encode($data);
?>
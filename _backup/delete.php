<?php
//header('Content-Type: application/json');
//include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
//include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
//$data = allowed_request_method('POST');
//
//if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;

    if(empty($_REQUEST['token'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "token"'];
        $errors++;
    }

    if(empty($_REQUEST['post_id'])) {
        $data = ['status' => 400, 'message' => 'Не введен: [GET] "post_id"'];
        $errors++;
    }

    $user_id = get_user_by_token($_REQUEST['token'], true);

    if(!$user_id) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует'];
        $errors++;
    }

    if($errors==0) {
        $post_id = $_REQUEST['post_id'];
        $query = $connect->query('SELECT * FROM post WHERE id = '.$post_id);
        if($query->num_rows > 0) {
            $post = $query->fetch_object();

            if($post->photo) {
                if(check_dir_of_file('post', $post->photo)) {
                    if (!unlink($_SERVER['DOCUMENT_ROOT'] . $post->photo)) {
                        $data = ['status' => 400, 'message' => 'Картинка статьи не удалена. Попробуйте ещё раз'];
                        $errors++;
                    }
                }
            }

            if($errors==0) {
                $data = ['status' => ($connect->query('DELETE FROM post WHERE id_user = '.$user_id.' AND id = '.$post_id)?200:500)];
            }
        } else {
            $data = ['status' => 400, 'message' => 'Статья не существует'];
        }
    }

    $connect->close();
//}
//
//echo json_encode($data);
?>
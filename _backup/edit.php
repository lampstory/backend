<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('PATCH');
$_REQUEST = json_request_data();

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $user = get_user_by_token($_REQUEST['token']);
    $errors = 0;

    if(!$user) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует'];
        $errors++;
    }

    if(empty($_REQUEST['post_id'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "post_id"'];
        $errors++;
    }

    $sql = $connect->query("SELECT * FROM post WHERE id = ".$_REQUEST['post_id']." AND id_user = ".$user->id);

    if ($sql->num_rows == 0) {
        $data = ['status' => 400, 'message' => 'Вы не можете редактировать данную статью'];
        $errors++;
    }

    if ($errors==0) {
        $i = 0;
        $post = $sql->fetch_object();
        $field = [];

        if($_REQUEST['name']) {
            array_push ($field, 'name = "'.$_REQUEST['name'].'"');
        }

        if($_REQUEST['text']) {
            array_push ($field, 'text = "'.$_REQUEST['text'].'"');
        }

        if($_REQUEST['password']) {
            array_push ($field, 'password = "'.md5($_REQUEST['password']).'"');
        }

        if($_FILES['photo']) {
            $photo = upload_file('post', 'photo');
            if(gettype($photo) <> 'string') {
                $data = $photo;
                $errors++;
            } else {
                array_push($field,'photo = "'.$photo.'"');
            }
        } else {
            array_push($field,'photo = "'.($_POST['photo']??"NULL").'"');
        }

        if($errors==0) {
            if($post->photo) {
                if(check_dir_of_file('post', $post->photo)) {
                    unlink($_SERVER['DOCUMENT_ROOT'] . $post->photo);
                }
            }

            $fields = implode(", ", $field);
            $data = ['status' => ($connect->query('UPDATE post SET '.$fields.' WHERE id = '.$post->id))?200:500];
        }
    }
    $connect->close();
}

echo json_encode($data);
?>
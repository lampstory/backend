<?
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/settings.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
page_setting('index', 'Апишка');
include_once $_SERVER['DOCUMENT_ROOT'].'/templates/navigation.php';

$db = new DataBase();
$connect = $db->connect();
$sql = $connect->query('SELECT api.*, api_section.name as section_name FROM api RIGHT JOIN api_section ON api_section.id = api.section_id ORDER BY api.section_id, api.ordering');
$user = get_user_by_token('29e8aa7909e4a59e78c8e5e1ec48c8ed');
?>

<div class="aligner">
    <?
    $prev = '';
    while($api = $sql->fetch_object()) {
        if($api->section_name <> $prev) {
            echo '<section class="api_table_section">'.$api->section_name.'</section>';
        }
        if($api->name <> null) {
        ?>
            <div class="api_table_row">

                <div class="api_column_name"><span><?=$api->name?></span></div>
                <div class="api_column_method"><span class="api_method"><?=$api->method?></span></div>
                <div class="api_column_description">
                    <span><? echo 'https://'.$_SERVER['HTTP_HOST'].$api->link; ?></span>
                    <div style="word-break: break-word"><?=$api->description?></div>
                </div>

                <div class="api_column_req">
                    <?
                    $required = '';
                    $sql2 = $connect->query('SELECT * FROM api_fields WHERE id_api = '.$api->id);
                    if($sql2->num_rows > 0) {
                        while($api_field = $sql2->fetch_object()) {
                            if($required <> $api_field->is_required) echo '<div class="'.($api_field->is_required?'required':'nonrequired').'">'.($api_field->is_required?'Обязательно':'Необязательно').'</div>';
                            echo '<span class="api_field">';
                                echo '<span class="api_field_name"><b>'.$api_field->name.'</b> - ';
                                echo $api_field->description.'</span>';
                            echo '</span>';
                            $required = $api_field->is_required;
                        }
                    }
                    ?>
                </div>
                <div class="api_column_login_required"><?=$api->is_login_required==1?'<i class="fas fa-user-shield"></i>Требуется вход':''?></div>
            </div>
        <?
        } else {
            echo '<div class="api_table_not_found">Информация ещё не существует</div>';
        }
        $prev = $api->section_name;
    }
    ?>

    <script type="text/javascript" async src="http://127.0.0.1:8000/static/widgets/widget-22c5781b4fa24c3384de0b21cac0a57f.js"></script>
    </div>

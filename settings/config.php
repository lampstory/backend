<?php
$conf = parse_ini_file('settings.ini');

define(ICON, $conf['icon']);
define(FIRST_TITLE, $conf['site_name_first']);
define(SECOND_TITLE, $conf['site_name_last']);

$GLOBALS = array(
    "server" => $conf['db_server'],
    "user" =>  $conf['db_user'],
    "password" =>  $conf['db_password'],
    "base" =>  $conf['db_base']
);

Class DataBase {
    private static $connect;
    private $server;
    private $user;
    private $password;
    private $base;

    public function __construct() {
        global $GLOBALS;
        $this->server = $GLOBALS['server'];
        $this->user =  $GLOBALS['user'];
        $this->password =  $GLOBALS['password'];
        $this->base =  $GLOBALS['base'];
    }

    public function connect() {
        @$this->connect = new mysqli($this->server, $this->user, $this->password, $this->base);
        if(!$this->connect->connect_errno){
            $this->connect->set_charset("utf8");
            return $this->connect;
        }
    }

    public function close() {
        $this->connect->close();
    }
}
?>

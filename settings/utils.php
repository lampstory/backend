<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/config.php';

function allowed_request_method($method) {
    return ($_SERVER['REQUEST_METHOD']==$method)?null:['status' => 400, 'message' => 'Разрешен метод: '.$method];
}

function request_data() {
    $data = file_get_contents('php://input');
    if(is_null(json_decode($data, TRUE))) {
        parse_str($data, $request);
    } else {
        $request = json_decode($data, TRUE);
    }
    return $request;
}

// Not using now
function request_data_raw(array &$_RAW) {
    $input = file_get_contents('php://input');
    preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
    $boundary = $matches[1];
    $a_blocks = preg_split("/-+$boundary/", $input);
    array_pop($a_blocks);
    foreach ($a_blocks as $id => $block) {
        if (empty($block)) continue;
        if (strpos($block, 'application/octet-stream') !== FALSE) {
            preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
        } else {
            preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
        }
        $_RAW[$matches[1]] = $matches[2];
    }
}

function get_user_by_token($token, $by_id=false) {
    $db = new DataBase();
    $connect = $db->connect();
    $query = "SELECT ".($by_id?'id':'*')." FROM user WHERE token = '".$token."'";
    $sql = $connect->query($query);
    $result = ($sql->num_rows > 0)?($by_id?$sql->fetch_object()->id:$sql->fetch_object()):false;
    if($by_id) {
        $settings = $connect->query('SELECT theme, color FROM user_settings WHERE id_user = '.$by_id);
        if ($settings->num_rows > 0) {
            $result->settings = $settings -> fetch_object();
        }
    }
    $connect->close();
    return $result;
}

function upload_file($folder, $file) {
    $errors = 0;
    $file_name = $_FILES[$file]['name'];
    $file_size = $_FILES[$file]['size'];
    $file_tmp = $_FILES[$file]['tmp_name'];
    $file_ext = strtolower(end(explode('.', $file_name)));

    if(in_array($file_ext, array("jpeg", "jpg", "png")) === false){
        $data = ['status' => 400, 'message' => 'Формат файла не JPEG или PNG'];
        $errors++;
    }

    if($file_size > 2097152*2){
        $data = ['status' => 400, 'message' => 'Размер файла превышает 4 МБ'];
        $errors++;
    }

    $file_upload = '/media/images/'.$folder.'/'.time().'.'.$file_ext;

    if($errors == 0){
        if(move_uploaded_file($file_tmp, $_SERVER['DOCUMENT_ROOT'].$file_upload)) {
            $data = $file_upload;
        } else {
            $data = ['status' => 400, 'message' => 'Файл не загружен'];
        }
    }

    return $data;
}

function check_dir_of_file($folder, $file) {
    return preg_match('/\/media\/images\/'.$folder.'/i', $file)?true:false;
}
?>
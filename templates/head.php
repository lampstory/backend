<head>
    <meta name="theme-color" content="#fff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="https://<? echo $_SERVER['HTTP_HOST']; ?>/media/css/main.css">
    <script src="https://kit.fontawesome.com/65d04e5aee.js"></script>
    <script src="https://<? echo $_SERVER['HTTP_HOST']; ?>/media/js/jquery.min.js"></script>
    <title><? echo $title; ?></title>
</head>
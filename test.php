<?
    include $_SERVER['DOCUMENT_ROOT'].'/templates/head.php';
?>

<div>
    <form id="add_post">
        <div>
            <p>Название:</p>
            <input type="text" name="name" required/>
        </div>

        <div>
            <p>Постер:</p>
            <div class='preview'>
                <img src="" id="img" style="max-width: 400px;">
            </div>
            <div>
                <input type="hidden" id="photo_link" name="photo"/>
                <input type="file" id="image" name="image"/>
                <input type="button" class="button" value="Загрузить" id="upload">
            </div>
        </div>

        <div>
            <p>Текст:</p>
            <textarea name="text" rows="15" style="width: 100%;"></textarea>
        </div>

        <div>
            <p>Категория:</p>
            <select class="tags" name="tag">
                <option selected>Не выбрано</option>
            </select>
        </div>

        <div>
            <p>Пароль:</p>
            <input type="text" id="password" name="password"/>
        </div>

        <div>
            <hr>
            <input type="hidden" name="token" value="<? echo $_COOKIE['sid']; ?>"/>
            <input type="hidden" id="post_id" name="post_id"/>
            <input type="button" id="post_draft" onclick="send_post(1);" value="Сохранить черновиком"/>
            <input type="button" id="post_save" onclick="send_post(0);" value="Создать пост"/>
        </div>


    </form>

    <script>
        // Cookie.js
        function getCookie(name) {
            var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
            return v ? v[2] : null;
        }
        function setCookie(name, value, days) {
            var d = new Date;
            d.setTime(d.getTime() + 24*60*60*1000*days);
            document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
        }

        function deleteCookie(name) {setCookie(name, '', -1); }

        // ----------------------------------------------------------------------------

        function send_post(is_draft) {
            post_id = $('#post_id').val();
            console.log(post_id);
            data = $('#add_post').serialize() + '&is_draft=' + is_draft;
            console.log(data);
            $.ajax({
                url: 'https://api.lampstory.site/v1/post/'+post_id,
                type: (post_id)?'PATCH':'POST',
                dataType : 'json',
                data: data,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status) {
                        console.log(response.status);
                        switch (response.status) {
                            case 200:
                                if(is_draft===1) {
                                    if(!post_id) $('#post_id').val(response.post_id);
                                    alert('Черновик сохранен');
                                } else {
                                    alert('Пост создан!');
                                    $('#add_post')[0].reset();
                                }
                                break;
                            case 400:
                                alert(response.message);
                                break;
                            case 500:
                                alert(response.message);
                                break;
                        }
                    }
                },
            });
        }

        function select_tags() {
            $.ajax({
                url: 'https://api.lampstory.site/v1/post/tags',
                type: 'GET',
                contentType: false,
                processData: false,
                success: function (response) {
                    for(var i = 0; i < Object.keys(response).length; i++) {
                        $('<option>', {
                            text: response[i].name,
                            value: response[i].id
                        }).appendTo('.tags');
                    }
                },
            });
        }

        $("#upload").click(function(){
            $('.preview').append('<div class="loading"><i class="fas fa-cog fa-spin"></i> Загрузка файла</div>');
            var fd = new FormData();
            var files = $('#image')[0].files[0];
            fd.append('image', files);
            fd.append('folder', 'post');
            $.ajax({
                url: 'https://api.lampstory.site/v1/_function/file.php',
                type: 'POST',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.status) {

                        if(response.status === 200) {
                            $("#img").attr("src", 'https://api.lampstory.site'+response.url);
                            $('#upload').val('Загрузить другую');
                            $('#photo_link').val(response.url);
                            $(".preview img").show();
                        }

                        if(response.status === 400) {
                            alert(response.message);
                        }
                    }
                    $('.loading').remove();
                },
            });
        });

        $(document).ready(function(){
            if(!getCookie('sid')) {
                setCookie('sid', '29e8aa7909e4a59e78c8e5e1ec48c8ed', 1);
            }
            select_tags();
        });
    </script>
</div>
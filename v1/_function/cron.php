<?php
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';

function select_all_files($files) {
    return array_diff(scandir($files), array('..', '.'));
}

function delete_unused_files($_ROOT, $connect) {
    $used_files = $delete_files = array();
    $folder = '/media/images/'.$_ROOT.'/';
    $dir = $_SERVER['DOCUMENT_ROOT'].$folder;
    $sql = $connect->query("SELECT photo FROM $_ROOT WHERE photo LIKE '%/media/images/$_ROOT/%'");
    // select used files
    if($sql->num_rows > 0) while($file = $sql->fetch_object()) array_push($used_files, $file->photo);
    if(!$connect->error) {
        // select unused files
        foreach (select_all_files($dir) as &$file) if(!in_array($folder.$file, $used_files)) array_push($delete_files, $dir.$file);
        // delete unused files
        if(!empty($delete_files)) foreach ($delete_files as $delete) unlink($delete);
    }
    echo $_ROOT.' unused files deleted --- '.PHP_EOL;
}

$db = new DataBase();
$connect = $db->connect();

foreach(array('post', 'user') as $root) delete_unused_files($root, $connect);

$connect->close();
?>
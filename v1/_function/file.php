<?
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$_METHOD = $_SERVER['REQUEST_METHOD'];
$folders = array('user', 'post');
$errors = 0;

switch ($_METHOD) {
    case 'POST':
        if (empty($_FILES['image'])) {
            $data = ['status' => 400, 'message' => 'Файл не был передан'];
            $errors++;
        }

        if (!in_array($_REQUEST['folder'], $folders)) {
            $data = ['status' => 400, 'message' => 'Не выбрана папка загрузки'];
            $errors++;
        }

        if ($errors == 0) {
            $photo = upload_file($_REQUEST['folder'], 'image');
            $data = (!is_array($photo)) ? ['status' => 200, 'url' => $photo] : $photo;
        }
        break;

    case 'DELETE':
        $_REQUEST = request_data();

        if (empty($_REQUEST['file'])) {
            $data = ['status' => 400, 'message' => 'Отсутствует ссылка на файл'];
            $errors++;
        }

        if($errors==0) {
            if(!check_dir_of_file($_REQUEST['file'])) {
                $data = ['status' => 400, 'message' => 'Файл не существует на сервер'];
                $errors++;
            }

            if($errors==0) {
                if (!unlink($_SERVER['DOCUMENT_ROOT'].$_REQUEST['file'])) {
                    $data = ['status' => 400, 'message' => 'Картинка статьи не удалена. Попробуйте ещё раз'];
                } else {
                    $data = ['status' => 200];
                }
            }
        }
        break;

    default:
        $data = ['status' => '400', 'message' => 'Разрешены только методы: POST, DELETE'];
}
echo json_encode($data);
?>
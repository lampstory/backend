<?
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('POST');
$_REQUEST = request_data();

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;
    $token = base64_decode($_REQUEST['token']);
    $user = get_user_by_token($token);

    if(!$user) $errors++;

    if($errors == 0) {
        if ($connect->query("UPDATE user SET is_active = 1 WHERE token = '".$token."'")) {
            $settings = $connect->query('SELECT theme, color FROM user_settings WHERE id_user = '.$user->id);
            if ($settings->num_rows > 0) $user->settings = $settings->fetch_object();
            $data = ['status' => 200, 'user' => $user];
        } else {
            $data = ["status" => 400, "message" => 'Аккаунта по данному токену не существует'];
        }
    }
}

echo json_encode($data);
?>
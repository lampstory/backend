<?
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/utils.php";
$data = allowed_request_method('POST');
$_REQUEST = request_data();

if(!$data) {
    $site_name = 'Ламповые истории';
    $subject = 'Активация аккаунта на сайте '.$site_name;
    $message = '
        <html>
        <head>
            <link rel="stylesheet" type="text/css" href="https://eugenemorozov.tk/font.css">
            <link rel="stylesheet" type="text/css" href="https://eugenemorozov.tk/style.css">
        </head>
        
        <body style="background: #222; padding: 100px 0; font-family: sans-serif;">
        <div style="display: block; width: 600px; margin: 0px auto; background: white; box-shadow: 0 2px 10px rgba(0,0,0,.3); padding: 20px;">
            <div>
                <div style="margin-top: 20px; font-size: 18px;">Добро пожаловать на сайт <b>'.($_REQUEST['fullname']?$_REQUEST['fullname']:'Безымянный пользователь, как так').'</b></div>
                <div style="background: #333; padding: 20px; margin-top: 20px;">
                    <table width="100%" border="0">
                        <tr>
                            <td style="font-size: 18px; color: #fff;" width="150px">Логин:</td>
                            <td style=" font-size: 18px;color: #f1f1f1; font-weight: bold;">'.($_REQUEST['username']?$_REQUEST['username']:'Логин потерялся').'</td>
                        </tr>
                    </table>
                </div>
                <div align="right">
                    <a href="http://'.$_REQUEST['site'].'/profile/activate/'.base64_encode($_REQUEST['token']).'" style="margin-top: 20px; font-size: 18px; display: inline-block; padding: 10px 20px; border-radius: 50px; text-transform: uppercase; font-size: 14px; color: white; background: #F44336; text-decoration: none; font-weight: bold;">
                        Активировать аккаунт
                    </a>
                </div>
            </div>
        </div>
        </body>
        </html>
    ';

    $mail_sender = 'verification@lampstory.site';
    $headers  = "From: ".$site_name." <".$mail_sender.">\n";
    $headers .= "Cc: ".$site_name." <".$mail_sender.">\n";
    $headers .= "X-Sender: ".$site_name." <".$mail_sender.">\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();
    $headers .= "X-Priority: 1\n";
    $headers .= "Return-Path: ".$mail_sender."\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=utf-8\n";

    if(mail($_REQUEST['email'], $subject, $message, $headers)) {
        $data = ["status" => 200, "message" => ['Отправлено', 'Письмо с кодом активации отправлено на Ваш email']];
    } else {
        $data = ["status" => 400, "message" => ['Ошибка', 'Письмо не отправлено']];
    }
}
echo json_encode($data);
?>
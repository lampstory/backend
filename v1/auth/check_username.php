<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('POST');
$_REQUEST = request_data();

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;

    $user = $connect->query("SELECT username FROM user WHERE username = '".$_REQUEST['username']."'");
    $data = ['status' => ($user->num_rows > 0) ? 400 : 200];
}

echo json_encode($data);
?>
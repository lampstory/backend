<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('POST');
$_REQUEST = request_data();

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;

    if(empty($_REQUEST['username'])) {
        $data = ['status' => 400, 'message' => 'Не введен никнейм пользователя'];
        $errors++;
    }

    if(empty($_REQUEST['password'])) {
        $data = ['status' => 400, 'message' => 'Не введен пароль'];
        $errors++;
    }

    $sql = $connect->query("SELECT * FROM user WHERE username = '".$_REQUEST['username']."'");

    if($sql->num_rows == 0) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует'];
        $errors++;
    } else {
        $user = $sql->fetch_object();
        if($user->password <> password_hash($_REQUEST['password'], PASSWORD_ARGON2I, ['salt' => md5(FIRST_TITLE.SECOND_TITLE)])) {
            $data = ['status' => 400, 'message' => 'Введен неправильный пароль'];
            $errors++;
        }
    }


    if($errors==0) {
        $token = $user->token;
        $settings = $connect->query('SELECT theme, color FROM user_settings WHERE id_user = '.$user->id);
        if ($settings->num_rows > 0) $user->settings = $settings->fetch_object();
        $data = ['status' => 200, 'token' => $token, 'user' => $user];
    }


    $connect->close();
    echo json_encode($data);
}
?>
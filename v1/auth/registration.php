<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('POST');
$_REQUEST = request_data();

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;

    if(empty($_REQUEST['username'])) {
        $data = ['status' => 400, 'message' => 'Не введен никнейм пользователя'];
        $errors++;
    }

    if(empty($_REQUEST['password'])) {
        $data = ['status' => 400, 'message' => 'Не введен пароль'];
        $errors++;
    }

    if(empty($_REQUEST['email'])) {
        $data = ['status' => 400, 'message' => 'Не введен email'];
        $errors++;
    }

    if(($connect->query("SELECT * FROM user WHERE username = '".$_REQUEST['username']."'")->num_rows) > 0) {
        $data = ['status' => 400, 'message' => 'Пользователь уже существует'];
        $errors++;
    }

    if(($connect->query("SELECT * FROM user WHERE email = '".$_REQUEST['email']."'")->num_rows) > 0) {
        $data = ['status' => 400, 'message' => 'Данный email уже используется'];
        $errors++;
    }

    if($errors==0) {
        $username = $_REQUEST['username'];
        $password = password_hash($_REQUEST['password'], PASSWORD_ARGON2I, ['salt' => md5(FIRST_TITLE.SECOND_TITLE)]);
        $fullname = $_REQUEST['fullname']??null;
        $about = $_REQUEST['about']??null;
        $email = $_REQUEST['email'];
        $token = md5($username.$password);
        $photo = "'".$_REQUEST['photo']."'"??"NULL";

        if($photo=="NULL" OR gettype($photo) == 'string') {
            if($connect->query("INSERT INTO user VALUES(DEFAULT, '$username', '$password', '$fullname', '$about', $photo, '$email', '$token', DEFAULT, DEFAULT)")) {
                $connect->query("INSERT INTO user_settings VALUES (DEFAULT , ".$connect->insert_id.", DEFAULT, DEFAULT)");
                $data = ['status' => 200, 'token' => $token];
            } else {
                $data = ['status' => 500];
            }
        }
    }


    $connect->close();
    echo json_encode($data);
}
?>
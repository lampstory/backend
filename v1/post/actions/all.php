<?
header('Access-Control-Allow-Origin: *');
$db = new DataBase();
$connect = $db->connect();
$errors = 0;
$sql = $connect->query("SELECT id, name, fulldate, password, id_user FROM post");

if ($sql->num_rows == 0) {
    $data = ['status' => 400, 'message' => 'Постов пока нет'];
    $errors++;
}

if ($errors==0) {
    $i = $j = 0;
    while ($post = $sql->fetch_object()) {
        $data[$i] = $post;
        $sql_tag = $connect->query('SELECT post_tag_info.name, post_tag_info.color FROM post_tags LEFT JOIN post_tag_info ON post_tag_info.id = post_tags.id_tag WHERE post_tags.id_post = '.$post->id);
        if($sql_tag->num_rows > 0) {
            while ($tag = $sql_tag->fetch_object()) {
                $data[$i]->tags[$j++] = $tag;
            }
            $j = 0;
        }
        $i++;
    }
}

$connect->close();
?>
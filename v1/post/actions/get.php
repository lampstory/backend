<?
$db = new DataBase();
$connect = $db->connect();
$errors = 0;
$sql = $connect->query("SELECT post.*, user.username as user, user.photo as user_photo FROM post LEFT JOIN user ON post.id_user = user.id WHERE post.id = ".$_GET['post_id']);

if(isset($_GET['token'])) {
    $user = get_user_by_token($_GET['token']);
    if(!$user) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует'];
        $errors++;
    }
}

if(empty($_GET['post_id'])) {
    $data = ['status' => 400, 'message' => 'Не введен: "post_id"'];
    $errors++;
}

if ($sql->num_rows == 0) {
    $data = ['status' => 400, 'message' => 'Пост не существует'];
    $errors++;
}

if ($errors==0) {
    $i = 0;
    $post = $sql->fetch_object();

    if($post->id_user <> $user->id) {
        if($post->password) {
            if(empty($_GET['password']) OR ($post->password <> $_GET['password'])) {
                $data = ['status' => 400, 'message' => 'Пароль введен неправильно '.$_GET['post_id'] ];
                $errors++;
            }
        }
    }

    if($errors==0) {
        if($post->photo) {
            if(check_dir_of_file('post', $post->photo)) {
                $post->photo = 'https://'.$_SERVER['HTTP_HOST'].$post->photo;
            }
        }
        if($post->user_photo) {
            if(check_dir_of_file('user', $post->user_photo)) {
                $post->user_photo = 'https://'.$_SERVER['HTTP_HOST'].$post->user_photo;
            }
        }

        $data = $post;
        $post_tags = $connect->query('SELECT post_tag_info.name, post_tag_info.color FROM post_tags LEFT JOIN post_tag_info ON post_tag_info.id = post_tags.id_tag WHERE post_tags.id_post = '.$post->id);

        if($post_tags->num_rows > 0) {
            $k = 0;
            while ($tag = $post_tags->fetch_object()) {
                $data->tags[$k] = $tag;
                $k++;
            }
        }
    }
}

$connect->close();
?>
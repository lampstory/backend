<?php
$db = new DataBase();
$connect = $db->connect();
$user = get_user_by_token($_REQUEST['token']);
$errors = 0;

if(!$user) {
    $data = ['status' => 400, 'message' => 'Пользователя не существует'];
    $errors++;
}

if(empty($_GET['post_id'])) {
    $data = ['status' => 400, 'message' => 'Не введен: "post_id"'];
    $errors++;
}

if($errors==0) {
    $sql = $connect->query("SELECT * FROM post WHERE id = ".$_GET['post_id']." AND id_user = ".$user->id);

    if ($sql->num_rows == 0) {
        $data = ['status' => 400, 'message' => 'Вы не можете редактировать данную статью'];
        $errors++;
    }

    if ($errors==0) {
        $i = 0;
        $post = $sql->fetch_object();
        $field = [];

        if(isset($_REQUEST['is_draft'])) {
            array_push ($field, 'is_draft = "'.$_REQUEST['is_draft'].'"');
        }

        if($_REQUEST['name']) {
            array_push ($field, 'name = "'.$_REQUEST['name'].'"');
        }

        if($_REQUEST['text']) {
            array_push ($field, 'text = "'.$_REQUEST['text'].'"');
        }

        if($_REQUEST['password']) {
            array_push ($field, 'password = "'.md5($_REQUEST['password']).'"');
        }

        if($_REQUEST['photo']) {
            array_push ($field, 'photo = "'.$_REQUEST['photo'].'"');
        }

        $fields = implode(", ", $field);
        $data = ['status' => ($connect->query('UPDATE post SET '.$fields.' WHERE id = '.$post->id))?200:500];
    }
}
$connect->close();
?>
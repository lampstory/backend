<?php
$db = new DataBase();
$connect = $db->connect();
$errors = 0;

if(empty($_REQUEST['token'])) {
    $data = ['status' => 400, 'message' => 'Не введен: "token"'];
    $errors++;
}

if(empty($_REQUEST['name'])) {
    $data = ['status' => 400, 'message' => 'Не введен: "name"'];
    $errors++;
}

if(empty($_REQUEST['text'])) {
    $data = ['status' => 400, 'message' => 'Не введен: "text"'];
    $errors++;
}

$user_id = get_user_by_token($_REQUEST['token'], true);

if(!$user_id) {
    $data = ['status' => 400, 'message' => 'Пользователя не существует'];
    $errors++;
}

if($errors==0) {
    $name = $_REQUEST['name'];
    $text = $_REQUEST['text'];
    $password = !empty($_REQUEST['password']) ? "'".md5($_REQUEST['password'])."'" : "DEFAULT";
    $tag = $_REQUEST['tag']??null;
    $photo = !empty($_REQUEST['photo'])? "'{$_REQUEST['photo']}'" : "DEFAULT";
    $id_draft = $_REQUEST['is_draft'];
    
    $post = $connect->query("INSERT INTO post VALUES(DEFAULT, '$name', DEFAULT, $photo, '$text', DEFAULT, $password, $id_draft, $user_id)")??false;

    if($post) {
        if($tag) {
            $post_id = $connect->insert_id;
            $tags = explode(",", $tag);
            foreach ($tags as $tag_id) {
                $connect->query('INSERT INTO post_tags VALUES(DEFAULT, '.$post_id.', '.$tag_id.')');
            }
        }

    }
    
    $data = $post ? ['status' => 200, 'post_id' => $connect->insert_id] : ['status' => 500];
}

$connect->close();
?>
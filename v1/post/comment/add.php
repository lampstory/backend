<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('POST');

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;

    if(empty($_POST['token'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "token"'];
        $errors++;
    }

    if(empty($_POST['text'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "text"'];
        $errors++;
    }

    if(empty($_GET['post_id'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "post_id"'];
        $errors++;
    }

    $user_id = get_user_by_token($_POST['token'], true);

    if(!$user_id) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует'];
        $errors++;
    }

    if($errors==0) {
        $text = $_POST['text'];
        $post_id = $_GET['post_id'];

        $query = $connect->query('SELECT * FROM post WHERE id = '.$post_id);
        if($query->num_rows > 0) {
            $data = ['status' => ($connect->query("INSERT INTO post_comment VALUES(DEFAULT, '$text', CURRENT_TIMESTAMP, $user_id, $post_id)"))?200:500];
        } else {
            $data = ['status' => 400, 'message' => 'Такого поста не существует'];
        }
    }

    $connect->close();
}

echo json_encode($data);
?>
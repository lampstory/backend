<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('POST');

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;

    if(empty($_POST['token'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "token"'];
        $errors++;
    }

    if(empty($_POST['text'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "text"'];
        $errors++;
    }

    if(empty($_REQUEST['post_id'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "post_id"'];
        $errors++;
    }

    if(empty($_REQUEST['comment_id'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "comment_id"'];
        $errors++;
    }

    $user_id = get_user_by_token($_POST['token'], true);

    if(!$user_id) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует'];
        $errors++;
    }

    if($errors==0) {
        $text = $_POST['text'];
        $post_id = $_REQUEST['post_id'];
        $comment_id = $_REQUEST['comment_id'];

        $query = $connect->query('SELECT * FROM post_comment WHERE id = '.$comment_id.' AND id_post='.$post_id);
        if($query->num_rows > 0) {
            $data = ['status' => ($connect->query("INSERT INTO post_comment_answer VALUES(DEFAULT, '$text', CURRENT_TIMESTAMP, $user_id, $comment_id)"))?200:500];
        } else {
            $data = ['status' => 400, 'message' => 'Статьи с таким комментарием не существует'];
        }
    }

    $connect->close();
}

echo json_encode($data);
?>
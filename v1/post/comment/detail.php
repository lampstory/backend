<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('GET');

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;
    $sql = $connect->query("SELECT * FROM post WHERE id = ".$_GET['post_id']);

    if(empty($_GET['post_id'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "post_id"'];
        $errors++;
    }

    if ($sql->num_rows == 0) {
        $data = ['status' => 400, 'message' => 'Пост не существует'];
        $errors++;
    }

    $comments = $connect->query('SELECT * FROM post_comment WHERE id_post = '.$_GET['post_id']);
    if($comments->num_rows==0) {
        $data = ['status' => 400, 'message' => 'У статьи нет комментариев'];
        $errors++;
    }

    if($errors==0) {
        $i = $j = 0;
        while($comment = $comments->fetch_object()) {
            $comment_answers = $connect->query('SELECT * FROM post_comment_answer WHERE id_comment = '.$comment->id);
            if($comment_answers->num_rows > 0) {
                while($comment_answer = $comment_answers->fetch_object()) {
                    $comment->answers[$j] = $comment_answer;
                    $j++;
                }
            }
            $data->comments[$i] = $comment;
            $i++;
        }
    }

    $connect->close();
}

echo json_encode($data);
?>
<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('GET');

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;
    $query = 'SELECT id FROM post';
    $query .= ($_GET['is_draft']) ? ' WHERE is_draft = 1' : ' WHERE is_draft = 0';
    $count = $connect->query($query)->num_rows;
    $data = ['count' => $count];
}

echo json_encode($data);
?>
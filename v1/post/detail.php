<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$_METHOD = $_SERVER['REQUEST_METHOD'];
$_REQUEST = request_data();

switch ($_METHOD) {
    case 'GET':
        include_once 'actions/get.php';
        break;
    case 'PATCH':
        include_once 'actions/patch.php';
        break;
    case 'DELETE':
        include_once 'actions/delete.php';
        break;
    default:
        $data = ['status' => '400', 'message' => 'Разрешены только методы: GET, PATCH, DELETE'];
}
echo json_encode($data);
?>
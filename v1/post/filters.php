<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('GET');

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;
    $query = "SELECT post.id, post.name, post.photo, post.fulldate, post.views, post.password, post.id_user, post.is_draft, user.username as user FROM post LEFT JOIN user ON post.id_user = user.id";

    if($_GET['search']) {
        $query .= " WHERE (name LIKE '%".$_GET['search']."%' OR username LIKE '%".$_GET['search']."%')";
    }

    if($_GET['year']) {
        $query .= " WHERE year(fulldate) = ".$_GET['year'];
    }

    if($_GET['user_id']) {
        $query .= " WHERE id_user = ".$_GET['user_id'];
    }

    if(isset($_GET['is_draft'])) {
        if($_GET['is_draft'] == 0 OR $_GET['is_draft']) {
            $state = ($_GET['search'] OR $_GET['year'] OR $_GET['user_id'])?'AND':'WHERE';
            $query .= ' '.$state.' is_draft = '.$_GET['is_draft'];
        }
    }
    
    if($_GET['sort']) {
        $sort = $_GET['sort'];
        switch ($sort) {
            case 'fulldate':
                $query .= " ORDER BY fulldate, id";
                break;
            case 'fulldate_desc':
                $query .= " ORDER BY fulldate DESC, id DESC";
                break;
        }
    }

    if($_GET['limit']) {
        $query .= " LIMIT " . (($_GET['limit'] <> 'start') ? $_GET['limit'] . ",": "") . " 12";
    }

    $sql = $connect->query($query);

    if ($sql->num_rows == 0) {
        $data = ['status' => 400, 'message' => 'Постов пока нет'];
        $errors++;
    }

    if ($errors==0) {
        $i = 0;
        while ($post = $sql->fetch_object()) {
            if(check_dir_of_file('post', $post->photo)) $post->photo = 'https://'.$_SERVER['HTTP_HOST'].$post->photo;
            $data[$i++] = $post;
        }
    }

    $connect->close();
}

echo json_encode($data);
?>
<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$_METHOD = $_SERVER['REQUEST_METHOD'];
$_REQUEST = request_data();

switch ($_METHOD) {
    case 'GET':
        include_once 'actions/all.php';
        break;
    case 'POST':
        include_once 'actions/post.php';
        break;
    default:
        $data = ['status' => '400', 'message' => 'Разрешены только методы: GET, POST'];
}

echo json_encode($data);
?>
<?
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('GET');

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;
    $query = "SELECT * FROM nav";

    $sql = $connect->query($query);

    if ($sql->num_rows == 0) {
        $data = ['status' => 400, 'message' => 'Навигации пока не существует'];
        $errors++;
    }

    if ($errors == 0) {
        $i = $j = 0;
        while ($nav = $sql->fetch_object()) {
            $data[$i] = $nav;
            $i++;
        }
    }
    $connect->close();
}
echo json_encode($data);
?>
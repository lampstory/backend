<?
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('GET');

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;
    $query = "SELECT id, username, fullname, about, email FROM user";

    if($_GET['limit']) {
        $query .= " LIMIT ".$_GET['limit'];
    }

    $sql = $connect->query($query);

    if ($sql->num_rows == 0) {
        $data = ['status' => 400, 'message' => 'Пользователей пока нет'];
        $errors++;
    }

    if ($errors == 0) {
        $i = $j = 0;
        while ($post = $sql->fetch_object()) {
            $data[$i] = $post;
            $i++;
        }
    }
    $connect->close();
}
echo json_encode($data);
?>
<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('POST');
$_REQUEST = request_data();

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;

    if(empty($_REQUEST['token'])) {
        $data = ['status' => 400, 'message' => 'Не введен: "token"'];
        $errors++;
    }

    $user = get_user_by_token($_REQUEST['token']);

    if(!$user) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует'];
        $errors++;
    }

    if($errors == 0) {
        if($user->photo) {
            if(check_dir_of_file('user', $user->photo)) {
                $user->photo = 'https://'.$_SERVER['HTTP_HOST'].$user->photo;
            }
        }
        $user->post_count = $connect->query('SELECT count(post.id) as post_count FROM post WHERE id = '.$user->id)->fetch_object()->post_count;
        $data = $user;
    }
    $connect->close();
}

echo json_encode($data);
?>
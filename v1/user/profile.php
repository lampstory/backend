<?php
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('GET');
$_REQUEST = request_data();

if(!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $errors = 0;

    $query = "SELECT user.username, user.fullname, user.about, user.photo, count(post.id) as post_count FROM user LEFT JOIN post ON user.id = post.id_user AND post.is_draft = 0 WHERE user.username = '".$_GET['username']."'";
    $user = $connect->query($query)->fetch_object();

    if(!$user->username) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует'];
        $errors++;
    }

    if($errors == 0) {
        $data = $user;
    }
    $connect->close();
}

echo json_encode($data);
?>
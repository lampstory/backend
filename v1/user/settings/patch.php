<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
include_once $_SERVER['DOCUMENT_ROOT']."/settings/config.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/utils.php';
$data = allowed_request_method('PATCH');
$_REQUEST = request_data();

if (!$data) {
    $db = new DataBase();
    $connect = $db->connect();
    $user = get_user_by_token($_REQUEST['token']);
    $field = [];
    $errors = 0;

    if(!$user) {
        $data = ['status' => 400, 'message' => 'Пользователя не существует '];
        $errors++;
    }

    if($errors==0) {
        if($_REQUEST['theme'] <> null) {
            array_push ($field, 'theme = '.intval($_REQUEST['theme']));
        }

        if($_REQUEST['color']) {
            array_push ($field, 'color = "'.$_REQUEST['color'].'"');
        }

        $fields = implode(", ", $field);
        $data = ['status' => ($connect->query('UPDATE user_settings SET '.$fields.' WHERE id_user = '.$user->id))?200:500];
    }
$connect->close();
}
echo json_encode($data);
?>